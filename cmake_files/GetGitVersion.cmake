function(git_get_version 
    RESULT_VERSION_MAJOR 
    RESULT_VERSION_MINOR 
    RESULT_VERSION_PATCH 
    RESULT_VERSION 
    RESULT_VERSION_SHORT
    RESULT_BRANCH)
  
  include(GetGitRevisionDescription)
  git_describe(VERSION --tags --dirty=-dirty)
  git_describe(BRANCH --all)

  MESSAGE( STATUS "VERSION:         " ${VERSION} )
  MESSAGE( STATUS "BRANCH:         " ${BRANCH} )

  #parse the version information into pieces.
  string(REGEX REPLACE "^v([0-9]+)\\..*" "\\1" VERSION_MAJOR "${VERSION}")
  string(REGEX REPLACE "^v[0-9]+\\.([0-9]+).*" "\\1" VERSION_MINOR "${VERSION}")
  string(REGEX REPLACE "^v[0-9]+\\.[0-9]+\\.([0-9]+).*" "\\1" VERSION_PATCH "${VERSION}")
  set(VERSION_SHORT "${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH}")

  set (${RESULT_VERSION_MAJOR} ${VERSION_MAJOR} PARENT_SCOPE)
  set (${RESULT_VERSION_MINOR} ${VERSION_MINOR} PARENT_SCOPE)
  set (${RESULT_VERSION_PATCH} ${VERSION_PATCH} PARENT_SCOPE)
  set (${RESULT_VERSION_SHORT} ${VERSION_SHORT} PARENT_SCOPE)
  set (${RESULT_VERSION} ${VERSION} PARENT_SCOPE)
  set (${RESULT_BRANCH} ${BRANCH} PARENT_SCOPE)

endfunction()