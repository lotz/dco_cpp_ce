//
// dco/c++/communityedition - Algorithmic Differentiation by Operator Overloading in C++
// Copyright (C) 2014-2016 K. Leppkes, J. Lotz, U. Naumann <info@stce.rwth-aachen.de>
//
// This file is part of dco/c++/communityedition.
//
// dco/c++/communityedition is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dco/c++/communityedition is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dco/c++/communityedition.  If not, see <http://www.gnu.org/licenses/>.
//

#include <iostream>
#include "types.hpp"

using namespace dco_cpp_ce;
using namespace std;

template <typename T>
  int f() {

  typename gt1s<T>::type x(2.0), y;
  derivative(x) = 1.0;
  
  y = -x;
  y = y * (+x) * (T)0.0;
  y = y * sin(x) + cos(x) + exp(x) + (T)0.0;
  y = y-x + y/x/((T)1.0);
  
  if (ceil(x) != ceil(value(x))) return 1;
#ifndef _MSC_VER
  if (floor(x) != floor(value(x))) return 1;
  if (isfinite(x) != isfinite(value(x))) return 1;
  if (isnan(x) != isnan(value(x))) return 1;
  if (isinf(x) != isinf(value(x))) return 1;
#endif
  
  y = min(x, y) + max(x, y) + min(x, 1.0) + max(x, 1.0);
  
  std::cout << "y = " << y << "; dy = " << derivative(y) << std::endl;
  
  bool a = (y < x) && (y == x) && (y >= x) && (y <= x) && (y != x);
  a  = (y < 1.0) && (1.0 == x);
  (void) a;

  return 0;
}

int main() {
  std::cout << "operations test" << std::endl;
  
  int s = 0;
  s += f<double>();
  s += f<gt1s<double>::type>();
  s += f<gt1s<gt1s<double>::type>::type>();
  s += f<gt1s<gt1s<gt1s<double>::type>::type>::type>();

  return s;
}
