//
// dco/c++/communityedition - Algorithmic Differentiation by Operator Overloading in C++
// Copyright (C) 2014-2016 K. Leppkes, J. Lotz, U. Naumann <info@stce.rwth-aachen.de>
//
// This file is part of dco/c++/communityedition.
//
// dco/c++/communityedition is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dco/c++/communityedition is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dco/c++/communityedition.  If not, see <http://www.gnu.org/licenses/>.
//

#include <iostream>
#include "types.hpp"

namespace dco = dco_cpp_ce;
using namespace dco;

int main() {
  std::cout << "interface test" << std::endl;
  
  gt1s<double>::type x;
  const gt1s<double>::type cx;
  double px;
  const double cpx = 1.0;
  
  value(x) = 1.0;
  derivative(x) = 1.0;
  std::cout << value(cx) << derivative(cx) << std::endl;

  value(px) = 1.0;
  derivative(px) = 1.0;
  std::cout << value(cpx) << derivative(cpx) << std::endl;


  if (mode<double>::is_dco_type == true) return 1;
  if (mode<gt1s<double>::type>::is_dco_type == false) return 1;
  if (mode<gt1s<gt1s<double>::type>::type>::is_dco_type == false) return 1;

  return 0;
}
