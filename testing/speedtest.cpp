//
// dco/c++/communityedition - Algorithmic Differentiation by Operator Overloading in C++
// Copyright (C) 2014-2016 K. Leppkes, J. Lotz, U. Naumann <info@stce.rwth-aachen.de>
//
// This file is part of dco/c++/communityedition.
//
// dco/c++/communityedition is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dco/c++/communityedition is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dco/c++/communityedition.  If not, see <http://www.gnu.org/licenses/>.
//

#include "types.hpp"
#include "timer.hpp"

namespace dco = dco_cpp_ce;
using namespace dco;

#include "f.hpp"

template<class T>
  inline void g(T *x, T* y) {
  T yl[2];
  const int N = 1e4;
  for (int n = 0; n < N; ++n) {
    f(x, yl);
    y[0] += yl[0] / N;
    y[1] += yl[1] / N;
  }
}

int main() {

  const double h = 1e-4;
  double y[2], dy_FD[2][5], dy_AD[2][5], ddy_FD[2][5][5], ddy_AD[2][5][5];

  double x[5];
  for (size_t i = 0; i < 5; ++i)
    x[i] = sin(static_cast<double>(i) + 1.0);

  // passive
  timer T; T.start();
  g(x, y);
  double passive = T.elapsed();
  std::cout << "passive = " << passive << std::endl;

  double FD_1st, FD_2nd, AD_1st, AD_2nd;
  
  // 1st derivative FD
  {
    double xh[5], yph[2], ymh[2];
    for (size_t i = 0; i < 5; ++i) xh[i] = x[i];
    T.start();
    for (size_t i = 0; i < 5; ++i) {
      xh[i] += h;
      g(xh, yph);
      xh[i] -= h;

      xh[i] -= h;
      g(xh, ymh);
      xh[i] += h;

      dy_FD[0][i] = (yph[0] - ymh[0])/(2*h);
      dy_FD[1][i] = (yph[1] - ymh[1])/(2*h);
    }
    FD_1st = T.elapsed();
    std::cout << "FD_1st = " << FD_1st << ", factor = " << FD_1st / passive << std::endl;
  }

  // 2nd derivative FD
  {
    double xh[5], ypph[2], ypmh[2], ymph[2], ymmh[2];
    for (size_t i = 0; i < 5; ++i) xh[i] = x[i];
    T.start();
    for (size_t i = 0; i < 5; ++i) {
      for (size_t j = 0; j < 5; ++j) {
        xh[i] += h;
        xh[j] += h;
        g(xh, ypph);
        xh[i] -= 2*h;
        g(xh, ymph);
        xh[j] -= 2*h;
        g(xh, ymmh);
        xh[i] += 2*h;
        g(xh, ypmh);
        xh[i] -= h;
        xh[j] -= h;
        ddy_FD[0][i][j] = (ypph[0] - ymph[0] - ypmh[0] + ymmh[0]) / (4*h*h);
        ddy_FD[1][i][j] = (ypph[1] - ymph[1] - ypmh[1] + ymmh[1]) / (4*h*h);
      }
    }
    FD_2nd = T.elapsed();
    std::cout << "FD_2nd = " << FD_2nd << FD_2nd / passive << ", factor = " << FD_2nd / passive << std::endl;
  }

  // 1st derivative AD
  {
    double error = 0.0;
    gt1s<double>::type ax[5], ay[2];
    for (size_t i = 0; i < 5; ++i) ax[i] = x[i];
    T.start();
    for (size_t i = 0; i < 5; ++i) {
      derivative(ax[i]) = 1.0;
      g(ax, ay);
      derivative(ax[i]) = 0.0;
      dy_AD[0][i] = derivative(ay[0]);
      dy_AD[1][i] = derivative(ay[1]);
      error += pow(dy_AD[0][i] - dy_FD[0][i], 2);
    }
    AD_1st = T.elapsed();
    std::cout << "error = " << error << std::endl;
    std::cout << "AD_1st = " << AD_1st << ", factor = " << AD_1st / passive << std::endl;
  }

  // 2nd derivative AD
  {
    double error = 0.0;
    gt1s<gt1s<double>::type>::type ax[5], ay[2];
    for (size_t i = 0; i < 5; ++i) ax[i] = x[i];
    T.start();
    for (size_t i = 0; i < 5; ++i) {
      for (size_t j = 0; j < 5; ++j) {
        derivative(value(ax[i])) = 1.0;
        value(derivative(ax[j])) = 1.0;
        g(ax, ay);
        derivative(value(ax[i])) = 0.0;
        value(derivative(ax[j])) = 0.0;
        ddy_AD[0][i][j] = derivative(derivative(ay[0]));
        ddy_AD[1][i][j] = derivative(derivative(ay[1]));
        error += pow(ddy_AD[0][i][j] - ddy_FD[0][i][j], 2);
      }
    }
    AD_2nd = T.elapsed();
    std::cout << "error = " << error << std::endl;
    std::cout << "AD_2nd = " << AD_2nd << ", factor = " << AD_2nd / passive << std::endl;
  }

  
  return 0;
}

