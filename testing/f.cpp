//
// dco/c++/communityedition - Algorithmic Differentiation by Operator Overloading in C++
// Copyright (C) 2014-2016 K. Leppkes, J. Lotz, U. Naumann <info@stce.rwth-aachen.de>
//
// This file is part of dco/c++/communityedition.
//
// dco/c++/communityedition is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dco/c++/communityedition is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dco/c++/communityedition.  If not, see <http://www.gnu.org/licenses/>.
//

#include "f.hpp"

template<class T> void f(T* x, T* _f) {
  T &_alpha = x[0];
  T &_beta  = x[1];
  T &_gamma = x[2];
  T &_beta2 = x[3];
  T &_beta3 = x[4];
 
  const T t1 = cos(_beta);
  const T t2 = cos(_alpha);
  const T t3 = sin(_beta2);
  const T t4 = t2 - t3;
  const T t5 = t2 + t3;
  const T t6 = t1*t1;
  const T t7 = t6 + 0.1e1*_beta3;
  const T t8 = cos(_gamma);
  const T t9 = sin(_gamma);
  const T t10 = t2*t2;
  const T t11 = 0.2e1 * t2;
  const T t12 = t10 + (-t11 - t3) * t3;
  const T t11_2 = t10 + (t11 - t3) * t3;
  const T t13 = 0.1e1 + (0.6e1 + t6) * t6;
  const T t14 = t9*t9;
  const T t15 = t8*t8;
  const T t16 = t15*t15 + t14*t14;
  const T t17 = t15 - t14;
  const T t18 = sqrt(0.5e1);
  const T t19 = sin(_beta);
  const T t20 = sqrt(0.3e1);
  const T t21 = sqrt(0.7e1);
  const T t22 = sqrt(0.14e2);
  const T t23 = sqrt(0.30e2);
  const T t24 = t8 - t9;
  const T t25 = t8 + t9;
  const T t26 = t19*t19;
  const T t27 = t26*t26;
  const T t28 = sqrt(0.2e1);
  const T t29 = sqrt(0.6e1);
  const T t30 = sqrt(0.42e2);
  const T t31 = 0.2e1 * t23;
  const T t32 = t18 * (0.4e1 * t20 + t28 * t29) * t30 + t31 * t20 * t21;
  const T t33 = t15 - t14 / 0.3e1;
  const T t34 = t15 - 0.3e1 * t14;
  const T t35 = t6 + 0.3e1;
  const T t36 = t12 * t11_2;
  const T t37 = t29 * t20;
  const T t38 = t28 + t37;
  const T t39 = -0.3e1 / 0.16e2;
  const T t40 = t15 + t14;
  const T t31_2 = t20 * (t6 * t18 * t29 + t31 * (t6 - t26 * t40 / 0.2e1)) * t28 + (0.12e2 * t18 + t29 * t23) * t6;
  const T t41 = t40 * t6;
  const T t42 = t41 - t15 - t14 - 0.2e1 * t26;
  const T t43 = sqrt(0.10e2);
  const T t44 = sqrt(0.15e2);
  const T t45 = t20 * t28;
  const T t46 = t45 * t42;
  const T t41_2 = t46 * t43 + (t41 - t15 - t14 - 0.8e1 * t26) * t44;
  const T t47 = t1 - 0.1e1;
  const T t48 = t1 + 0.1e1;
  const T t30_2 = _alpha*t41_2 * sqrt(0.105e3) - 0.30e2 * t30 * t26 * t29 + 0.15e2 * t40 * t47 * t48 * t21;
  const T t49 = _beta*t3*t3;
  const T t50 = t10*t10 + (-0.6e1 * t10 + t49) * t49*_beta2;
  const T t51 = 0.4e1 * t2 * t3;
  const T t37_2 = (t45 * (t6 - 0.3e1 / 0.8e1 * t26 * t40) + t29 * t6 / 0.2e1) * t44 + (-t37 * t26 * t40 / 0.2e1 + 0.3e1 / 0.2e1 * t28 * (t6 - 0.5e1 / 0.4e1 * t26 * t40)) * t18;
  const T t44_2 = sqrt(0.70e2);
  const T t52 = sqrt(0.21e2);
  const T t10_2 = t51 * (t10 - t49)*_beta3;
  const T t49_2 = t50 * t1;
  const T t53 = t18 / 0.1680e4*_beta2;
  const T t28_2 = t53 * ((0.6e1 * t28 * t44_2 * t43 + 0.15e2 * t52 * t29) * t48 * t47 * t40 + t46 * t44_2 * t23 + t41_2 * sqrt(0.210e3));
  const T t44_3 = t1 * t21 / 0.6e1*_beta*_gamma;
  const T t46_2 = -t26 * t40 / 0.4e1;
  const T t53_2 = t26 * t25 * t24 * t31_2 * t21 / 0.24e2 - t53 * t30_2 * (t36 * t7 * t25 * t24 - 0.16e2 * t2 * t8 * t3 * t9 * t4 * t5 * t1);
  const T t54 = t19 * (t1 * t32 * t8 * t26 * t34 * t21 / 0.96e2 - t38 * ((-0.36e2 * t6 - 0.12e2) * t33 * t9 * t5 * t4 * t3 * t2 + t36 * t8 * t34 * t1 * t35) * t18 / 0.16e2);
  const T t55 = 0.2e1 * t8;
  const T t55_2 = (-0.384e3 * t8 * t9 * t17 * t7 * t5 * t4 * t3 * t2 * t1 + t36 * t13 * (0.6e1 * t16 - 0.36e2 * t14 * t15)) * t18 / 0.48e2 + t27 * t20 * t21 * t22 * t23 * (t15 + (-t55 - t9) * t9) * (t15 + (t55 - t9) * t9) / 0.48e2;
  
  const T _f_0 = ((0.24e2 * t16 - 0.144e3 * t14 * t15) * t7 * t5 * t4 * t3 * t2 * t1 + 0.6e1 * t9 * t12 * t11_2 * t13 * t8 * t17) * t18 / 0.12e2 + t27 * t8 * t9 * t20 * t21 * t22 * t23 * t24 * t25 / 0.12e2;
  const T _f_1 = t19 * (t9 * t1 * t26 * t32 * t33 * t21 / 0.32e2 + t39 * ((0.4e1 * t6 + 0.4e1 / 0.3e1) * t34 * t8 * t5 * t4 * t3 * t2 + t36 * t9 * t33 * t1 * t35) * t38 * t18);
  const T _f_2 = t21 * t9 * t31_2 * t26 * t8 / 0.12e2 - t30_2 * (t7 * t50 * t9 * t8 + t51 * t24 * t25 * t4 * t5 * t1) * t18 / 0.840e3;
  const T _f_3 = t19 * t44_3 * t9 * t37_2 + t28_2 * (t49_2 * t9 + t10_2 * t8);
  const T _f_4 = -t36 * (-t40 * t47 * t41_2 * t48 * t52 / 0.2e1 + t26 * (t23 * t42 * t20 + 0.6e1 * t43 * t40 * t47 * t48) * t22) * t18 / 0.420e3 + t21 * (t46_2 * t45 * t29 * (t6 + t46_2) + (t6 - 0.3e1 / 0.2e1 * t26 * t40) * t6);
  const T _f_5 = t19 * (t44_3 * t37_2 * t8 + t28_2 * (t49_2 * t8 - t10_2 * t9));
  const T _f_6 = t53_2;
  const T _f_7 = t54*1;
  const T _f_8 = t55_2;

  
  _f[0] = _f_0 + _f_1 + _f_2 + _f_3 + _f_4 + _f_5 + _f_6 + _f_7 + _f_8;
  _f[1] = _f_0 * _f_1; 
}

template void f<double>
  (double*,
   double*);
template void f<dco_cpp_ce::gt1s<double>::type>
  (dco_cpp_ce::gt1s<double>::type*,
   dco_cpp_ce::gt1s<double>::type*);
template void f<dco_cpp_ce::gt1s<dco_cpp_ce::gt1s<double>::type>::type>
  (dco_cpp_ce::gt1s<dco_cpp_ce::gt1s<double>::type>::type*,
   dco_cpp_ce::gt1s<dco_cpp_ce::gt1s<double>::type>::type*);

